COUCHDB_CONTAINER ?= couchdb_test
COUCHDB_UN ?= admin
COUCHDB_PW ?= adminpw
COUCHDB_PORT ?= 5984
COUCHDB_VER ?= 3.2

start-couchdb:
	@echo "start couchdb......"
	@docker run -d --name ${COUCHDB_CONTAINER} \
	-p 5984:5984 \
	-e COUCHDB_USER=${COUCHDB_UN} \
	-e COUCHDB_PASSWORD=${COUCHDB_PW} \
	couchdb:${COUCHDB_VER}
	@echo "localhost:${COUCHDB_PORT} ${COUCHDB_UN}:${COUCHDB_PW}"

stop-couchdb:
	docker rm -f ${COUCHDB_CONTAINER}

.PHONY: start-couchdb stop-couchdb