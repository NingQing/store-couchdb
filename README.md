# store-couchdb

## couchdb api

```golang
type config struct {
	host          string
	port          string
	user          string
	password      string
	timeOut       time.Duration // 超时时间  s
	maxRetries    int  // 最大重试次数
	retryWaitTime time.Duration // 重试等待时间 ms 下次等待时间 = 这次等待时间*2
}

type Client struct {
	ctx    context.Context
	dbName string
	conf   *config
	client *http.Client
	logger protocol.Logger
	cancel context.CancelFunc
}
```

- db_name 数据库名称
- doc_id 文档ID
- startkey 开始键
- endkey 结束键
- limit 查询数量

### 创建数据库
- PUT /{db_name}
- `CreateDatabaseIfNotExist()` 如果数据库不存在则创建。

### 检查文档
- HEAD /{db_name}/{doc_id}
- `getDocRev` 获取文档最新版本号，如若不存在则返回空
- `Has` 查看文档是否存在

### 插入文档
- PUT /{db_name}/{doc_id}
- Body `{"data": "value1" }`
- `Put` 插入一个文档，先获取文档最新版本号，如果不存在直接插入，存在则更新最新版本
### 冲突处理
如果发生冲突，重新获取版本号，重新插入，只尝试一次

### 读取文档
- GET /{db_name}/{doc_id}
- `Get` 获取最新版本的文档

### 删除文档
- DELETE /{db_name}/{doc_id}
- `Delete` 删除最新版本的文档，需要提供文档版本号
#### 冲突处理
暂未实现，假设当前删除的是v1，但是由于更新变成v2，那是否要删除v2

### 批量获取
- POST /{db_name}/_all_docs?include_docs=true
- Body `{"keys":["key1","key2"]}`
- `GetKeys` 批量获取文档

### 批量插入
- POST /{db_name}/_bulk_docs
- Body `{"docs":[{"_id":"key1","_rev":"v1","_deleted":true},{"_id":"key2","_rev":"v1"}]}`
- `BatchUpdate` 批量更新，如果没有版本号则是插入，有版本号则是更新，`_deleted`为true则是删除
#### 冲突处理
发生冲突，对冲突文档进行重试一次

### 遍历
- GET /{db_name}/_all_docs?startkey={startkey}&endkey={endkey}&limit={limit}&inclusive_end=false&include_docs=true
- `RangeKeys` 遍历文档, 范围[startkey,endkey), size = limit

#### 通过前缀遍历
- POST /{db_name}/_find
- Body `{"selector":{"_id":{"$regex":"key+"}},"fields":["_id","_rev","data"]}`
- `RangePrefix` 通过前缀遍历文档，前缀通过正则表达式匹配

### 重试及超时
```go
    // ....... 
    waitDuration := c.conf.retryWaitTime
	for {
		select {
		case <-ctx.Done():
            // 超时
			return nil, CancelErr
		default:
            // ......
            if maxRetries-1 < 1 {
                // 错误处理
	        }
	        maxRetries--
	        time.Sleep(waitDuration)
	        waitDuration *= 2
        }
    }
```

## 参考文档
- [couchdb document HTTP API](https://docs.couchdb.org/en/3.2.2-docs/api/index.html)