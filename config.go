package storecouchdb

type CouchDBConfig struct {
	Host          string `mapstructure:"host"`
	Port          int    `mapstructure:"port"`
	Prefix        string `mapstructure:"prefix"`
	User          string `mapstructure:"user"`
	Password      string `mapstructure:"password"`
	TimeOut       int    `mapstructure:"time_out"`
	MaxRetries    int    `mapstructure:"max_retries"`
	RetryWaitTime int    `mapstructure:"retry_wait_time"`
}
