package couchdb_test

import (
	"context"
	"regexp"
	"testing"

	"chainmaker.org/chainmaker/protocol/v2/test"
	"chainmaker.org/chainmaker/store-couchdb/v2/couchdb"
	"github.com/stretchr/testify/assert"
)

func TestCreateDatabaseIfNotExist(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	err := client.CreateDatabaseIfNotExist()
	assert.Nil(t, err)
}

func TestPut(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	rev, err := client.Put("key1", []byte("value1"))
	assert.Nil(t, err)
	assert.NotNil(t, rev)
}

func TestGet(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	raw, err := client.Get("key1")
	assert.Nil(t, err)
	assert.Equal(t, "value1", string(raw))
}

func TestDelete(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	ok, err := client.Delete("key1")
	assert.Nil(t, err)
	assert.True(t, ok)
}

func TestHas(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	exist, err := client.Has("key1")
	assert.Nil(t, err)
	assert.False(t, exist)
}

func TestBatchUpdate(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	b := couchdb.NewBatch()
	b.Put("key1", []byte("value1"))
	b.Put("key2", []byte("value2"))
	b.Put("key3", []byte("value3"))
	b.Put("key4", []byte("value4"))
	b.Put("key5", []byte("value5"))
	b.Put("key6", []byte("value6"))
	b.Put("key7", []byte("value7"))
	b.Put("key8", []byte("value8"))
	b.Put("key9", []byte("value9"))
	err := client.BatchUpdate(b)
	assert.Nil(t, err)
}

func TestGetKeys(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	keys := []string{"key1", "key2", "key3", "key4", "key5"}
	values := [][]byte{[]byte("value1"), []byte("value2"), []byte("value3"), []byte("value4"), []byte("value5")}
	list, err := client.GetKeys(keys)
	assert.Nil(t, err)
	assert.Equal(t, values, list)
}

func TestRangeKeys(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	keys := []string{"key1", "key2", "key3", "key4", "key5"}
	docs, err := client.RangeKeys("key1", "key9", 5)
	assert.Nil(t, err)
	assert.Len(t, docs, 5)
	for _, doc := range docs {
		assert.Contains(t, keys, doc.ID)
	}
}

func TestRangePrefix(t *testing.T) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	docs, err := client.RangePrefix("key")
	assert.Nil(t, err)
	for _, doc := range docs {
		assert.Regexp(t, regexp.MustCompile("key+"), doc.ID)
	}
}
