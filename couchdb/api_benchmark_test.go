package couchdb_test

import (
	"context"
	"strconv"
	"testing"

	"chainmaker.org/chainmaker/protocol/v2/test"
	"chainmaker.org/chainmaker/store-couchdb/v2/couchdb"
)

func BenchmarkPut(b *testing.B) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	for n := 0; n < b.N; n++ {
		client.Put("b_key"+strconv.Itoa(n), []byte("value"+strconv.Itoa(n)))
	}
}

func BenchmarkGet(b *testing.B) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	for n := 0; n < b.N; n++ {
		client.Get("b_key" + strconv.Itoa(n))
	}
}

func BenchmarkDelete(b *testing.B) {
	client := couchdb.NewCouchDB(context.Background(), couchdb.WitchLogger(&test.GoLogger{}))
	for n := 0; n < b.N; n++ {
		client.Delete("b_key" + strconv.Itoa(n))
	}
}
