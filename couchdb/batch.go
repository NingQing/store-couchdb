package couchdb

import (
	"unicode/utf8"

	"github.com/pkg/errors"
)

type Batch struct {
	data  map[string]interface{}
	index []string
}

// NewBatch
func NewBatch() *Batch {
	return &Batch{
		data:  make(map[string]interface{}, 0),
		index: make([]string, 0),
	}
}

// Put 
func (b *Batch) Put(key string, value []byte) error {
	if !utf8.ValidString(key) {
		return errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	b.data[key] = map[string]interface{}{
		"_id":  key,
		"data": value,
	}
	b.index = append(b.index, key)
	return nil
}

// Delete
func (b *Batch) Delete(key string) error {
	if !utf8.ValidString(key) {
		return errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	b.data[key] = map[string]interface{}{
		"_id":      key,
		"_deleted": true, // 删除
	}
	b.index = append(b.index, key)
	return nil
}

// Index
func (b *Batch) Index() []string {
	return b.index
}

func (b *Batch) setRev(rev map[string]string) {
	for k, v := range b.data {
		if rev[k] == "" {
			continue
		}
		v.(map[string]interface{})["_rev"] = rev[k]
	}
}

func (b *Batch) structured() []interface{} {
	result := make([]interface{}, 0)
	for _, v := range b.data {
		result = append(result, v)
	}
	return result
}

func (b *Batch) isDelete(key string) bool {
	_, ok := b.data[key].(map[string]interface{})["_deleted"]
	return ok
}

func (b *Batch) value(key string) []byte {
	return b.data[key].(map[string]interface{})["data"].([]byte)
}
