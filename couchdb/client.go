package couchdb

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"chainmaker.org/chainmaker/protocol/v2"
)

const (
	DefaultDBName        = "test_db"
	DefaultHost          = "localhost"
	DefaultPort          = 5984
	DefaultUser          = "admin"
	DefaultPassword      = "adminpw"
	DefaultTimeOut       = 5
	DefaultMaxRetries    = 3
	DefaultRetryWaitTime = 500
)

type Option func(c *Client)

type config struct {
	host          string
	port          int
	user          string
	password      string
	timeOut       time.Duration
	maxRetries    int
	retryWaitTime time.Duration
}

type Client struct {
	ctx    context.Context
	dbName string
	conf   *config
	logger protocol.Logger
	cancel context.CancelFunc
}

// NewCouchDB
func NewCouchDB(ctx context.Context, options ...Option) *Client {
	c, cancel := context.WithCancel(ctx)
	client := &Client{
		ctx:    c,
		dbName: DefaultDBName,
		conf: &config{
			host:          DefaultHost,
			port:          DefaultPort,
			user:          DefaultUser,
			password:      DefaultPassword,
			timeOut:       DefaultTimeOut * time.Second,
			maxRetries:    DefaultMaxRetries,
			retryWaitTime: DefaultRetryWaitTime * time.Millisecond,
		},
		logger: nil,
		cancel: cancel,
	}
	for _, option := range options {
		option(client)
	}
	return client
}

func (c *Client) Close() {
	c.cancel()
}

// WithDBName set database name, default test_db.
func WithDBName(name string) Option {
	return func(c *Client) {
		c.dbName = name
	}
}

// WithHost set database host, default loaclhost.
func WithHost(host string) Option {
	return func(c *Client) {
		c.conf.host = host
	}
}

// WithPort set database port, default 5984.
func WithPort(port int) Option {
	return func(c *Client) {
		c.conf.port = port
	}
}

// WithUser set database username, default admin.
func WithUser(user string) Option {
	return func(c *Client) {
		c.conf.user = user
	}
}

// WithPassword set database password, default adminpw.
func WithPassword(password string) Option {
	return func(c *Client) {
		c.conf.password = password
	}
}

// WithTimeout set database connection timeout, default 5s.
func WithTimeout(t int) Option {
	return func(c *Client) {
		if t > 0 {
			c.conf.timeOut = time.Duration(t) * time.Second
		}
	}
}

// WithMaxRetries set maximum number of request retries, default 3.
func WithMaxRetries(num int) Option {
	return func(c *Client) {
		if num > 0 {
			c.conf.maxRetries = num
		}
	}
}

// WithRetryWaitTime set 1th wait time when retrying, default 500ms.
// the next wait time is twice as long as this time.
func WithRetryWaitTime(t int) Option {
	return func(c *Client) {
		if t > 0 {
			c.conf.retryWaitTime = time.Duration(t) * time.Millisecond
		}
	}
}

// WitchLogger set client logger.
func WitchLogger(logger protocol.Logger) Option {
	return func(c *Client) {
		c.logger = logger
	}
}

// UrlString return url.
func (c *Client) UrlString() string {
	URL := &url.URL{
		Host:   fmt.Sprintf("%s:%d", c.conf.host, c.conf.port),
		Scheme: "http",
	}
	return URL.String()
}

// Url return url.
func (c *Client) Url() *url.URL {
	return &url.URL{
		Host:   fmt.Sprintf("%s:%d", c.conf.host, c.conf.port),
		Scheme: "http",
	}
}
