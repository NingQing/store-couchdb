package couchdb

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
)

// GetDatabaseInfo gets information about the specified database.
// GET /{db}
// StatusCode 200 404
func (c *Client) GetDatabaseInfo() (*DBInfo, error) {
	resp, err := c.handleRequestWithoutRev(http.MethodGet, c.Url(), nil, nil)
	if err != nil {
		if err == ErrorNotFound {
			c.logger.Debugf("[%s] Database does not exist.", c.dbName)
			return nil, nil
		}
		return nil, err
	}
	defer closeResponseBody(resp)
	dbResponse := &DBInfo{}
	decodeErr := json.NewDecoder(resp.Body).Decode(&dbResponse)
	if decodeErr != nil {
		return nil, errors.Wrap(decodeErr, "error decoding response body")
	}

	c.logger.Debugf("get database info: %s", dbResponse)
	return dbResponse, nil
}

// CreateDatabase if the database does not exist, it is created.
// the database name must be composed by following next rules
// - Name must begin with a lowercase letter (a-z)
// - Lowercase characters (a-z)
// - Digits (0-9)
// - Any of the characters , , , , , , and ._$()+-/
// PUT /{db}
// StatusCode 201 400 401
func (c *Client) CreateDatabaseIfNotExist() error {
	c.logger.Debugf("if not exist,create database [%s]", c.dbName)
	dbInfo, err := c.GetDatabaseInfo()
	if err != nil {
		return err
	}
	if dbInfo == nil {
		c.logger.Debugf("create database [%s]", c.dbName)
		resp, err := c.handleRequestWithoutRev(http.MethodPut, c.Url(), nil, nil)
		if err != nil {
			dbInfo, err := c.GetDatabaseInfo()
			if err == nil && dbInfo == nil {
				return err
			}
		}
		defer closeResponseBody(resp)
		c.logger.Infof("created database [%s]", c.dbName)
	} else {
		c.logger.Debugf("[%s] Database already exists", c.dbName)
	}
	return nil
}
