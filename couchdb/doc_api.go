package couchdb

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"unicode/utf8"

	"github.com/pkg/errors"
)

// HEAD /{db}/{docid} return document revision
func (c *Client) getDocRev(key string) (string, error) {
	resp, err := c.handleRequestWithoutRev(http.MethodHead, c.Url(), nil, nil, key)
	if err != nil {
		if err == ErrorNotFound {
			c.logger.Debugf("[%s] not found (404), returning nil value instead of 404 error", key)
			return "", nil
		}
		return "", err
	}
	defer closeResponseBody(resp)
	revision, err := getRevHeader(resp)
	if err != nil {
		return "", err
	}
	return revision, nil
}

// POST 
func (c *Client) getDocsRev(keys []string) (map[string]string, error) {
	jsonMap := make(map[string]interface{})
	jsonMap["keys"] = keys
	raw, err := json.Marshal(jsonMap)
	if err != nil {
		return nil, errors.Wrap(err, "error marshalling json data")
	}
	resp, err := c.handleRequestWithoutRev(http.MethodPost, c.Url(), raw, nil, "_all_docs")
	if err != nil {
		return nil, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "error reading response body")
	}
	response := &allDocsResponse{}
	if err := json.Unmarshal(data, response); err != nil {
		return nil, errors.Wrap(err, "error unmarshalling json data")
	}
	results := make(map[string]string, 0)
	for _, row := range response.Rows {
		results[row.GetKey()] = row.GetRev()
	}
	return results, nil
}

// Put  插入文档
// 如果存在文档就更新，更新文档可能因为存在冲突而失败
// 如果文档不存在就直接插入
// PUT /{db}/{doc_id}
func (c *Client) Put(key string, value []byte) (string, error) {
	if !utf8.ValidString(key) {
		return "", errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	// 获取文档版本号
	rev, err := c.getDocRev(key)
	if err != nil {
		return "", err
	}
	jsonMap := make(map[string]interface{})
	jsonMap["data"] = value
	raw, err := json.Marshal(jsonMap)
	if err != nil {
		return "", errors.Wrap(err, "error marshalling json data")
	}
	resp, err := c.handleRequestWithoutDefault(http.MethodPut, c.Url(), raw, rev, nil, key)
	if err != nil {
		if err == ErrorConflict {
			c.logger.Debugf("[%s] document revision conflict detected", key)
			return "", err
		}
		return "", err
	}
	defer closeResponseBody(resp)

	revision, err := getRevHeader(resp)
	if err != nil {
		return "", err
	}
	return revision, nil
}

type getDocResponse struct {
	ID   string `json:"_id"`
	Rev  string `json:"_rev"`
	Data []byte `json:"data"`
}

// Get 获取文档
// 如果没有找到文档则返回 (nil,nil)
// GET /{db}/{doc_id}
func (c *Client) Get(key string) ([]byte, error) {
	if !utf8.ValidString(key) {
		return nil, errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	resp, err := c.handleRequestWithoutRev(http.MethodGet, c.Url(), nil, nil, key)
	if err != nil {
		if err == ErrorNotFound {
			c.logger.Debugf("[%s] not found (404), returning nil value instead of 404 error", key)
			return nil, nil
		}
		return nil, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "error reading response body")
	}
	c.logger.Debugf("response: %s", string(data))
	result := &getDocResponse{}
	if err := json.Unmarshal(data, result); err != nil {
		return nil, errors.Wrap(err, "error unmarshalling json data")
	}
	return result.Data, nil
}

type deleteDocResponse struct {
	ID       string `json:"id"`
	OK       bool   `json:"ok"`
	Revision string `json:"rev"`
}

// Delete 删除一个文档
// 删除成功和文档不存在都返回返回true
// 删除失败返回false
// DELETE /{db}/{doc_id}
func (c *Client) Delete(key string) (bool, error) {
	if !utf8.ValidString(key) {
		return false, errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	rev, err := c.getDocRev(key)
	if err != nil {
		return false, err
	}
	resp, err := c.handleRequestWithoutDefault(http.MethodDelete, c.Url(), nil, rev, nil, key)
	if err != nil {
		if err == ErrorNotFound {
			c.logger.Debugf("[%s] not found (404), returning nil value instead of 404 error", key)
			return true, nil
		}
		return false, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, errors.Wrap(err, "error reading response body")
	}
	result := &deleteDocResponse{}
	if err := json.Unmarshal(data, result); err != nil {
		return false, errors.Wrap(err, "error unmarshalling json data")
	}
	return result.OK, nil
}

// Has 文档是否存在
// 如果存在则返回true
// 不存在和发生错误都会返回false
func (c *Client) Has(key string) (bool, error) {
	if !utf8.ValidString(key) {
		return false, errors.Errorf("doc id [%x] not a valid utf8 string", key)
	}
	rev, err := c.getDocRev(key)
	if err != nil {
		return false, err
	}
	if rev == "" {
		return false, nil
	}
	return true, nil
}

// GetKeys 批量获取文档
// 不存在的返回nil
// POST /{db}/_all_docs?include_docs=true
func (c *Client) GetKeys(keys []string) ([][]byte, error) {
	// TODO 检查keys
	jsonMap := make(map[string]interface{})
	jsonMap["keys"] = keys
	raw, err := json.Marshal(jsonMap)
	if err != nil {
		return nil, errors.Wrap(err, "error marshalling json data")
	}

	getKeysURL := c.Url()
	queryParms := getKeysURL.Query()
	queryParms.Add("include_docs", "true")

	resp, err := c.handleRequestWithoutRev(http.MethodPost, getKeysURL, raw, &queryParms, "_all_docs")
	if err != nil {
		return nil, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "error reading response body")
	}
	response := &allDocsResponse{}
	if err := json.Unmarshal(data, response); err != nil {
		return nil, errors.Wrap(err, "error unmarshalling json data")
	}

	var results [][]byte
	for _, row := range response.Rows {
		results = append(results, row.GetValue())
	}
	return results, nil
}

type batchUpdateResponse struct {
	ID     string `json:"id"`
	Error  string `json:"error"`
	Reason string `json:"reason"`
	Ok     bool   `json:"ok"`
	Rev    string `json:"rev"`
}

func (c *Client) BatchUpdate(batch *Batch) error {
	rev, err := c.getDocsRev(batch.Index())
	if err != nil {
		return err
	}
	batch.setRev(rev)
	jsonDocsMap := make(map[string]interface{})
	jsonDocsMap["docs"] = batch.structured()
	raw, err := json.Marshal(jsonDocsMap)
	fmt.Println(string(raw))
	if err != nil {
		return errors.Wrap(err, "error marshalling json data")
	}
	resp, err := c.handleRequestWithoutRev(http.MethodPost, c.Url(), raw, nil, "_bulk_docs")
	if err != nil {
		return err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "error reading response body")
	}
	response := []batchUpdateResponse{}
	err = json.Unmarshal(data, &response)
	if err != nil {
		return errors.Wrap(err, "error unmarshalling json data")
	}

	for _, resp := range response {
		if resp.Ok {
			continue
		}
		// TODO 冲突处理
		if batch.isDelete(resp.ID) {
			c.Delete(resp.ID)
			continue
		}
		c.Put(resp.ID, batch.value(resp.ID))
	}
	return nil
}

func getRevHeader(resp *http.Response) (string, error) {
	if resp == nil {
		return "", errors.New("no response received from CouchDB")
	}

	revision := resp.Header.Get("Etag")

	if revision == "" {
		return "", errors.New("no revision tag detected")
	}

	reg := regexp.MustCompile(`"([^"]*)"`)
	revisionNoQuotes := reg.ReplaceAllString(revision, "${1}")
	return revisionNoQuotes, nil
}

func genBulkGetQuery(keys []string) ([]map[string]interface{}, error) {
	results := make([]map[string]interface{}, 0)
	for _, key := range keys {
		if !utf8.ValidString(key) {
			return nil, errors.Errorf("doc id [%x] not a valid utf8 string", key)
		}
		results = append(results, map[string]interface{}{
			"id": key,
		})
	}
	return results, nil
}
