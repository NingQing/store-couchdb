package couchdb

import "errors"

var (
	ErrorCancel       = errors.New("proactive cancellation")
	ErrorBadRequest  = errors.New("status 400,the request provided invalid JSON data")
	ErrorUnauthorized = errors.New("status 401, read permission required")
	ErrorNotFound     = errors.New("status 404, requested data not found")
	ErrorConflict     = errors.New("status 409, document with the specified ID already exists or specified revision is not latest for target document")
	ErrorServer      = errors.New("status 500,  internal server error or timeout")
)
