package couchdb

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func (c *Client) handleRequest(ctx context.Context, method string, connectURL *url.URL, data []byte, rev string,
	keepConnectionOpen bool, maxRetries int, queryParms *url.Values, pathElements ...string) (*http.Response, error) {

	requestURL := constructCouchDBUrl(connectURL, c.dbName, pathElements...)

	payloadData := new(bytes.Buffer)
	payloadData.ReadFrom(bytes.NewReader(data))

	return c.send(ctx, method, requestURL, payloadData, rev, keepConnectionOpen, maxRetries, queryParms)
}

func (c *Client) handleRequestWithoutRev(method string, connectURL *url.URL, data []byte, queryParms *url.Values,
	pathElements ...string) (*http.Response, error) {

	ctx, _ := context.WithTimeout(c.ctx, c.conf.timeOut)

	return c.handleRequest(ctx, method, connectURL, data, "", false, c.conf.maxRetries, queryParms, pathElements...)
}

func (c *Client) handleRequestWithoutDefault(method string, connectURL *url.URL, data []byte, rev string, queryParms *url.Values,
	pathElements ...string) (*http.Response, error) {

	ctx, _ := context.WithTimeout(c.ctx, c.conf.timeOut)

	return c.handleRequest(ctx, method, connectURL, data, rev, false, c.conf.maxRetries, queryParms, pathElements...)
}

func (c *Client) send(ctx context.Context, method string, requestURL *url.URL, body io.Reader, rev string,
	keepConnectionOpen bool, maxRetries int, queryParms *url.Values) (*http.Response, error) {

	if queryParms != nil {
		requestURL.RawQuery = queryParms.Encode()
	}

	waitDuration := c.conf.retryWaitTime
	retries := 0
	for {
		select {
		case <-ctx.Done():
			return nil, ErrorCancel
		default:
			c.logger.Debugf("%dth retry,[%s] %s", retries+1, method, requestURL.String())
			response, err := handleRequest(ctx, method, requestURL, body, rev, keepConnectionOpen, queryParms, c.conf.user, c.conf.password)
			if err != nil {
				if err == ErrorNotFound {
					return nil, err
				}
				if retries < maxRetries {
					c.logger.Debugf("an error has occurred, %s, wait %.2f and try again", err.Error(), waitDuration.Seconds())
					time.Sleep(waitDuration)
					waitDuration *= 2
					retries++
					continue
				}
				return nil, err
			}
			return response, nil
		}
	}
}

func handleRequest(ctx context.Context, method string, requestURL *url.URL, body io.Reader, rev string,
	keepConnectionOpen bool, queryParms *url.Values, username string, password string) (*http.Response, error) {

	var resp *http.Response
	var errResp error

	req, err := http.NewRequestWithContext(ctx, method, requestURL.String(), body)
	if err != nil {
		return nil, errors.Wrap(err, "error creating http request")
	}

	// 如果有附件需求，且当附件长度为0时不应保持连接
	if keepConnectionOpen {
		req.Close = true
	}

	if rev != "" {
		req.Header.Set("If-Match", rev)
	}

	if method == http.MethodPut || method == http.MethodPost || method == http.MethodDelete {
		req.Header.Set("Content-Type", "application/json")
		if rev != "" {
			req.Header.Set("If-Match", rev)
		}
	}
	// 无附件模式下  响应都要求为json
	if method == http.MethodGet || method == http.MethodPut || method == http.MethodPost || method == http.MethodDelete {
		req.Header.Set("Accept", "application/json")
	}

	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}

	resp, errResp = http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(errResp, "handel requets error")
	}

	if req == nil {
		return nil, errors.New("unable to connect to CouchDB, check the hostname and port")
	}
	if err := handleResponeErr(resp); err != nil {
		return nil, err
	}
	return resp, nil
}

func handleResponeErr(resp *http.Response) error {
	switch resp.StatusCode {
	case 200:
		return nil
	case 201:
		return nil
	case 400:
		return ErrorBadRequest
	case 404:
		return ErrorNotFound
	case 409:
		return ErrorConflict
	default:
		return ErrorServer
	}
}

func closeResponseBody(resp *http.Response) {
	if resp != nil {
		io.Copy(ioutil.Discard, resp.Body)
		resp.Body.Close()
	}
}

func constructCouchDBUrl(connectURL *url.URL, dbName string, pathElements ...string) *url.URL {
	var buffer bytes.Buffer
	buffer.WriteString(connectURL.String())
	if dbName != "" {
		buffer.WriteString("/")
		buffer.WriteString(encodePathElement(dbName))
	}
	for _, pathElement := range pathElements {
		buffer.WriteString("/")
		buffer.WriteString(encodePathElement(pathElement))
	}
	return &url.URL{Opaque: buffer.String()}
}

func encodePathElement(str string) string {
	u := &url.URL{}
	u.Path = str
	encodedStr := u.EscapedPath()
	encodedStr = strings.Replace(encodedStr, "/", "%2F", -1)
	encodedStr = strings.Replace(encodedStr, "+", "%2B", -1)

	return encodedStr
}
