package couchdb

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/pkg/errors"
)

func (c *Client) RangeKeys(startKey, endKey string, limit int) ([]Doc, error) {
	var err error

	rangeURL := c.Url()
	queryParms := rangeURL.Query()
	if limit > 0 {
		queryParms.Set("limit", strconv.Itoa(limit))
	}
	queryParms.Add("include_docs", "true")
	queryParms.Add("inclusive_end", "false")

	if startKey != "" {
		if startKey, err = encodeForJSON(startKey); err != nil {
			return nil, err
		}
		queryParms.Add("startkey", "\""+startKey+"\"")
	}

	if endKey != "" {
		var err error
		if endKey, err = encodeForJSON(endKey); err != nil {
			return nil, err
		}
		queryParms.Add("endkey", "\""+endKey+"\"")
	}

	resp, err := c.handleRequestWithoutRev(http.MethodGet, rangeURL, nil, &queryParms, "_all_docs")
	if err != nil {
		return nil, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "error reading response body")
	}
	response := &allDocsResponse{}
	if err := json.Unmarshal(data, response); err != nil {
		return nil, errors.Wrap(err, "error unmarshalling json data")
	}

	var results []Doc
	for _, row := range response.Rows {
		if row.GetValue() != nil {
			results = append(results, row.Doc)
		}
	}
	return results, nil
}

func (c *Client) RangePrefix(prefix string) ([]Doc, error) {
	selectorField := make(map[string]interface{}, 0)
	selectorField["_id"] = map[string]interface{}{
		"$regex": prefix + "+",
	}
	fieldsField := []string{"_id", "_rev", "data"}
	jsonMap := make(map[string]interface{})
	jsonMap["selector"] = selectorField
	jsonMap["fields"] = fieldsField
	raw, err := json.Marshal(jsonMap)
	if err != nil {
		return nil, errors.Wrap(err, "error marshalling json data")
	}
	resp, err := c.handleRequestWithoutRev(http.MethodPost, c.Url(), raw, nil, "_find")
	if err != nil {
		return nil, err
	}
	defer closeResponseBody(resp)

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "error reading response body")
	}
	response := &findDocsResponse{}
	if err := json.Unmarshal(data, response); err != nil {
		return nil, errors.Wrap(err, "error unmarshalling json data")
	}

	var results []Doc
	for _, doc := range response.Docs {
		if len(doc.Data) != 0 {
			results = append(results, doc)
		}
	}

	return results, nil
}

func encodeForJSON(str string) (string, error) {
	buf := &bytes.Buffer{}
	encoder := json.NewEncoder(buf)
	if err := encoder.Encode(str); err != nil {
		return "", errors.Wrap(err, "error encoding json data")
	}
	buffer := buf.Bytes()
	return string(buffer[1 : len(buffer)-2]), nil
}
