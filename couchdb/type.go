package couchdb

import (
	"encoding/json"
)

type DBInfo struct {
	DbName string `json:"db_name"`
	Sizes  struct {
		File     int `json:"file"`
		External int `json:"external"`
		Active   int `json:"active"`
	} `json:"sizes"`
	Other struct {
		DataSize int `json:"data_size"`
	} `json:"other"`
	DocDelCount       int    `json:"doc_del_count"`
	DocCount          int    `json:"doc_count"`
	DiskSize          int    `json:"disk_size"`
	DiskFormatVersion int    `json:"disk_format_version"`
	DataSize          int    `json:"data_size"`
	CompactRunning    bool   `json:"compact_running"`
	InstanceStartTime string `json:"instance_start_time"`
}

func (info *DBInfo) String() string {
	b, _ := json.Marshal(info)
	return string(b)
}

type allDocsResponse struct {
	TotalRows int   `json:"total_rows"`
	Offset    int   `json:"offset"`
	Rows      []row `json:"rows"`
}

type row struct {
	ID    string `json:"id"`
	Error string `json:"error"`
	Key   string `json:"key"`
	Value struct {
		Revision string `json:"rev"`
	} `json:"value"`
	Doc Doc `json:"Doc"`
}

type findDocsResponse struct {
	Docs []Doc `json:"docs"`
}

type Doc struct {
	ID       string `json:"_id"`
	Revision string `json:"_rev"`
	Data     []byte `json:"data"`
}

func (r *row) GetKey() string {
	return r.Key
}

func (r *row) GetRev() string {
	return r.Value.Revision
}

func (r *row) GetValue() []byte {
	if len(r.Doc.Data) != 0 {
		return r.Doc.Data
	}
	return nil
}
