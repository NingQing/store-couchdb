package storecouchdb

import (
	"context"
	"fmt"
	"strings"
	"time"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store-couchdb/v2/couchdb"
	"github.com/pkg/errors"
)

var _ protocol.DBHandle = (*CouchDBHandle)(nil)

type CouchDBHandle struct {
	db             *couchdb.Client
	logger         protocol.Logger
	encryptor      crypto.SymmetricKey
	writeBatchSize uint64
}

type CouchDBOptions struct {
	Config    *CouchDBConfig
	Logger    protocol.Logger
	Encryptor crypto.SymmetricKey
	Prefix    string
	ChainId   string
	DBName    string
}

func NewCouchDBHandle(input *CouchDBOptions) *CouchDBHandle {
	dbname := strings.ToLower(fmt.Sprintf("%s%s_%s", input.Prefix, input.ChainId, input.DBName))
	db := couchdb.NewCouchDB(context.Background(),
		couchdb.WithDBName(dbname),
		couchdb.WithHost(input.Config.Host),
		couchdb.WithPort(input.Config.Port),
		couchdb.WithUser(input.Config.User),
		couchdb.WithPassword(input.Config.Password),
		couchdb.WithMaxRetries(input.Config.MaxRetries),
		couchdb.WithRetryWaitTime(input.Config.RetryWaitTime),
		couchdb.WithTimeout(input.Config.TimeOut),
		couchdb.WitchLogger(input.Logger),
	)
	err := db.CreateDatabaseIfNotExist()
	if err != nil {
		panic(fmt.Sprintf("Error opening %s [%s] by couchdbprovider: %s", db.UrlString(), fmt.Sprintf("%s_%s", input.ChainId, input.DBName), err))
	}

	return &CouchDBHandle{
		db:             db,
		logger:         input.Logger,
		encryptor:      input.Encryptor,
		writeBatchSize: 128,
	}
}

func (h *CouchDBHandle) GetDbType() string {
	return "couchdb"
}

func (h *CouchDBHandle) Get(key []byte) ([]byte, error) {
	value, err := h.db.Get(string(key))
	if err != nil {
		h.logger.Errorf("getting couchdbprovider key [%s], err:%s", key, err.Error())
		return nil, err
	}
	if value == nil {
		return nil, nil
	}
	if h.encryptor != nil && len(value) > 0 {
		return h.encryptor.Decrypt(value)
	}
	return value, nil
}

func (h *CouchDBHandle) GetKeys(keys [][]byte) ([][]byte, error) {
	var err error
	var args []string
	for _, v := range keys {
		args = append(args, string(v))
	}
	results, err := h.db.GetKeys(args)
	if err != nil {
		h.logger.Errorf("getting couchdbprovider [%d] keys, first key [%s], err:%s", len(args), args[0], err.Error())
		return nil, errors.Wrapf(err, "error getting couchdbprovider [%d] keys", len(args))
	}
	if h.encryptor != nil {
		for i, max := 0, len(results); i < max; i++ {
			if len(results[i]) > 0 {
				results[i], err = h.encryptor.Decrypt(results[i])
				if err != nil {
					return nil, err
				}

			}
		}
	}

	return results, nil
}

func (h *CouchDBHandle) Put(key []byte, value []byte) error {
	var err error
	if value == nil {
		h.logger.Warn("writing couchdbdbprovider key [%s] with nil value", key)
		return errors.New("error writing couchdbdbprovider with nil value")
	}
	if h.encryptor != nil && len(value) > 0 {
		value, err = h.encryptor.Encrypt(value)
		if err != nil {
			return err
		}
	}
	_, err = h.db.Put(string(key), value)
	if err != nil {
		h.logger.Errorf("writing couchdbprovider key [%s]", key)
		return errors.Wrapf(err, "error writing couchdbprovider key [%s]", key)
	}
	return err
}

func (h *CouchDBHandle) Has(key []byte) (bool, error) {
	exist, err := h.db.Has(string(key))
	if err != nil {
		h.logger.Errorf("getting couchdbprovider key [%s], err:%s", key, err.Error())
		return false, errors.Wrapf(err, "error getting couchdbprovider key [%s]", key)
	}
	return exist, nil
}

func (h *CouchDBHandle) Delete(key []byte) error {
	ok, err := h.db.Delete(string(key))
	if err != nil {
		h.logger.Errorf("deleting couchdbprovider key [%s]", key)
		return errors.Wrapf(err, "error deleting couchdbprovider key [%s]", key)
	}

	if !ok {
		h.logger.Errorf("deleting couchdbprovider key [%s]", key)
		return errors.Wrapf(errors.New("delet faild"), "error deleting couchdbprovider key [%s]", key)
	}
	return nil
}

func (h *CouchDBHandle) WriteBatch(batch protocol.StoreBatcher, _ bool) error {
	var err error
	start := time.Now()
	if batch.Len() == 0 {
		return nil
	}
	b := couchdb.NewBatch()
	for k, v := range batch.KVs() {
		if v == nil {
			b.Delete(k)
		} else {
			if h.encryptor != nil && len(v) > 0 {
				v, err = h.encryptor.Encrypt(v)
				if err != nil {
					return err
				}
			}
			b.Put(k, v)
		}
	}
	batchFilterDur := time.Since(start)
	err = h.db.BatchUpdate(b)
	if err != nil {
		h.logger.Errorf("write batch to couchdbprovider failed")
		return errors.Wrap(err, "error writing batch to couchdbprovider")
	}
	writeDur := time.Since(start)
	h.logger.Debugf("couchdb write batch[%d] sync: none, time used: (filter:%d, write:%d, total:%d)",
		batch.Len(), batchFilterDur.Milliseconds(), (writeDur - batchFilterDur).Milliseconds(),
		time.Since(start).Milliseconds())
	return nil
}

func (h *CouchDBHandle) CompactRange(start, limit []byte) error {
	return nil
}

func (h *CouchDBHandle) NewIteratorWithRange(startKey []byte, limitKey []byte) (protocol.Iterator, error) {
	return NewCouchDBIteratorWithRange(h.db, startKey, limitKey, 0)
}

func (h *CouchDBHandle) NewIteratorWithPrefix(prefix []byte) (protocol.Iterator, error) {
	return NewCouchDBIteratorWithPrefix(h.db, prefix)
}

func (h *CouchDBHandle) GetWriteBatchSize() uint64 {
	return h.writeBatchSize
}

func (h *CouchDBHandle) Close() error {
	h.db.Close()
	return nil
}


