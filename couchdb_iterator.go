package storecouchdb

import (
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store-couchdb/v2/couchdb"
)

var _ protocol.Iterator = (*CouchDBIterator)(nil)

type CouchDBIterator struct {
	index  int
	length int
	data   []couchdb.Doc
}

func NewCouchDBIteratorWithRange(db *couchdb.Client, start []byte, end []byte, limit int) (*CouchDBIterator, error) {
	docs, err := db.RangeKeys(string(start), string(end), limit)
	if err != nil {
		return nil, err
	}
	return &CouchDBIterator{
		index:  -1,
		length: len(docs),
		data:   docs,
	}, nil
}

func NewCouchDBIteratorWithPrefix(db *couchdb.Client, prefix []byte) (*CouchDBIterator, error) {
	docs, err := db.RangePrefix(string(prefix))
	if err != nil {
		return nil, err
	}
	return &CouchDBIterator{
		index:  -1,
		length: len(docs),
		data:   docs,
	}, nil
}

func (i *CouchDBIterator) Next() bool {
	i.index++
	return i.index < i.length
}

func (i *CouchDBIterator) First() bool {
	i.index = 0
	return true
}

func (i *CouchDBIterator) Error() error {
	// TODO
	return nil
}

func (i *CouchDBIterator) Key() []byte {
	return []byte(i.data[i.index].ID)
}

func (i *CouchDBIterator) Value() []byte {
	if len(i.data[i.index].Data) > 0 {
		return i.data[i.index].Data
	}
	return nil
}

func (i *CouchDBIterator) Release() {
	// TODO
}
