package storecouchdb_test

import (
	"testing"

	"chainmaker.org/chainmaker/common/v2/crypto/sym/sm4"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/test"
	storecouchdb "chainmaker.org/chainmaker/store-couchdb/v2"
	"github.com/stretchr/testify/assert"
)

var EncryptoEnable = true

func init() {
	config := &storecouchdb.CouchDBConfig{
		Host:          "localhost",
		Port:          5984,
		User:          "admin",
		Password:      "adminpw",
		TimeOut:       5,
		MaxRetries:    3,
		RetryWaitTime: 500,
	}
	options := &storecouchdb.CouchDBOptions{
		Config:  config,
		Logger:  &test.GoLogger{},
		ChainId: "chain",
		DBName:  "test_db",
	}
	if EncryptoEnable {
		key := &sm4.SM4Key{Key: []byte("1234567890abcdef")}
		options.Encryptor = key
	}
	dbHandle = storecouchdb.NewCouchDBHandle(options)
}

type UpdateBatch struct {
	kvs map[string][]byte
}

func (batch *UpdateBatch) Get(key []byte) ([]byte, error) {
	//TODO implement me
	panic("implement me")
}

func (batch *UpdateBatch) Has(key []byte) bool {
	//TODO implement me
	panic("implement me")
}

func (batch *UpdateBatch) SplitBatch(batchCnt uint64) []protocol.StoreBatcher {
	panic("implement me")
}

// NewUpdateBatch constructs an instance of a Batch
func NewUpdateBatch() protocol.StoreBatcher {
	return &UpdateBatch{kvs: make(map[string][]byte)}
}

// KVs return map
func (batch *UpdateBatch) KVs() map[string][]byte {
	return batch.kvs
}

// Put adds a KV
func (batch *UpdateBatch) Put(key []byte, value []byte) {
	if value == nil {
		panic("Nil value not allowed")
	}
	batch.kvs[string(key)] = value
}

// Delete deletes a Key and associated value
func (batch *UpdateBatch) Delete(key []byte) {
	batch.kvs[string(key)] = nil
}

// Len returns the number of entries in the batch
func (batch *UpdateBatch) Len() int {
	return len(batch.kvs)
}

// Merge merges other kvs to this updateBatch
func (batch *UpdateBatch) Merge(u protocol.StoreBatcher) {
	for key, value := range u.KVs() {
		batch.kvs[key] = value
	}
}

var dbHandle *storecouchdb.CouchDBHandle

func TestGetDbType(t *testing.T) {
	assert.Equal(t, "couchdb", dbHandle.GetDbType())
}

func TestPut(t *testing.T) {
	err := dbHandle.Put([]byte("key1"), []byte("value1"))
	assert.Nil(t, err)
}

func TestGet(t *testing.T) {
	value, err := dbHandle.Get([]byte("key1"))
	assert.Nil(t, err)
	assert.Equal(t, "value1", string(value))
}

func TestWriteBatch(t *testing.T) {
	batch := NewUpdateBatch()
	batch.Put([]byte("key1"), []byte(""))
	batch.Put([]byte("key2"), []byte("value2"))
	batch.Put([]byte("key3"), []byte("value3"))
	batch.Put([]byte("key4"), []byte("value4"))
	batch.Put([]byte("key5"), []byte("value5"))

	err := dbHandle.WriteBatch(batch, false)
	assert.Nil(t, err)
}

func TestGetKeys(t *testing.T) {
	keys := [][]byte{[]byte("key1"), []byte("key2"), []byte("key3")}
	values, err := dbHandle.GetKeys(keys)
	assert.Nil(t, err)
	assert.Equal(t, [][]byte{nil, []byte("value2"), []byte("value3")}, values)
}

func TestDelete(t *testing.T) {
	err := dbHandle.Delete([]byte("key2"))
	assert.Nil(t, err)
}

func TestNewCouchDBIteratorWithRange(t *testing.T) {
	if EncryptoEnable {
		return
	}
	iterator, err := dbHandle.NewIteratorWithRange([]byte("key3"), []byte("key4"))
	assert.Nil(t, err)
	var count int
	var keys, values [][]byte
	if EncryptoEnable {
		key := &sm4.SM4Key{Key: []byte("1234567890abcdef")}
		iterator = storecouchdb.NewEncryptedIterator(*iterator.(*storecouchdb.CouchDBIterator), key)
	}
	for iterator.Next() {
		count++
		keys = append(keys, iterator.Key())
		values = append(values, iterator.Value())
	}
	assert.Equal(t, 1, count)
	assert.Equal(t, [][]byte{[]byte("key3")}, keys)
	assert.Equal(t, [][]byte{[]byte("value3")}, values)
}

func TestNewCouchDBIteratorWithPrefix(t *testing.T) {

	iterator, err := dbHandle.NewIteratorWithPrefix([]byte("key"))
	assert.Nil(t, err)
	var count int
	var keys, values [][]byte

	if EncryptoEnable {
		key := &sm4.SM4Key{Key: []byte("1234567890abcdef")}
		iterator = storecouchdb.NewEncryptedIterator(*iterator.(*storecouchdb.CouchDBIterator), key)
	}

	for iterator.Next() {
		count++
		keys = append(keys, iterator.Key())
		values = append(values, iterator.Value())
	}
	assert.Equal(t, 3, count)
	assert.Equal(t, [][]byte{[]byte("key3"), []byte("key4"), []byte("key5")}, keys)
	assert.Equal(t, [][]byte{[]byte("value3"), []byte("value4"), []byte("value5")}, values)

	var key, value []byte
	if iterator.First() {
		key = iterator.Key()
		value = iterator.Value()
	}
	assert.Equal(t, []byte("key3"), key)
	assert.Equal(t, []byte("value3"), value)

}
