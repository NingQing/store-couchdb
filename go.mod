module chainmaker.org/chainmaker/store-couchdb/v2

go 1.16

require (
	chainmaker.org/chainmaker/protocol/v2 v2.3.0
	github.com/pkg/errors v0.9.1
)

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	github.com/stretchr/testify v1.7.0
)
